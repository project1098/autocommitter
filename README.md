## Autocomitter
A program that will commit to github repository at a certain time.\
Purpose: Fill in the blanks in gituhub contributions graph, and to reduce suffering for people with perfectionism, but not enough free time to make commits every day.\

How it should work: The program will detect wether commits were made during the day. If not, it will create something, commit and push it to github, therefore fill the vacancies in the commit graph.

